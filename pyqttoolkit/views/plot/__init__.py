"""plotview

This module defines the plotview used in the application
"""
from .toolbar import PlotToolbarWidget
from .tool_type import ToolType
from .plot_toolbar_options import PlotToolbarOptions
from .size import ToggleableFixed, MaxWidth
from .plot_options import PlotOptionsView
from .legend_control import LegendControlView
