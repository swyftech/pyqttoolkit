from enum import Enum

class ToolType(Enum):
    zoom = 0
    polygon = 1
    span = 2
    pan = 3
    reset = 4
    options = 5
    legend = 6
